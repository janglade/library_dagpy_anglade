<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        $lastID = $this->pdo->lastInsertId();

        for ($i=0; $i < $copies; $i++) { 
            $query = $this->pdo->prepare('INSERT INTO exemplaires (book_id) VALUES (?)');
            $this->execute($query, array($lastID));
        }
    }

    /**
     * Inserting a book borrowing in the database
     */
    public function insertBorrow($name, $copy, $end)
    {
        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($name, $copy, date('Y-m-d'), $end));
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting a book
     */
    public function getBook($id)
    {
        $query = $this->pdo->prepare('SELECT livres.*
                                        FROM livres
                                        WHERE livres.id = ?');

        $this->execute($query, array($id));

        return $query->fetchAll();
    }

    /**
     * Getting availables copies of a book
     */
    public function getAvailablesCopies($id)
    {
        $query = $this->pdo->prepare('SELECT exemplaires.*
                                        FROM exemplaires
                                        INNER JOIN livres ON livres.id = exemplaires.book_id
                                        WHERE livres.id = ?
                                        AND exemplaires.id NOT IN (SELECT emprunts.exemplaire FROM emprunts
                                                                     WHERE emprunts.fini = 0)');

        $this->execute($query, array($id));

        return $query->fetchAll();
    }

    /**
     * Getting unavailables copies of a book
     */
    public function getUnavailablesCopies($id)
    {
        $query = $this->pdo->prepare('SELECT exemplaires.*
                                        FROM exemplaires
                                        INNER JOIN livres ON livres.id = exemplaires.book_id
                                        WHERE livres.id = ?
                                        AND exemplaires.id IN (SELECT emprunts.exemplaire FROM emprunts
                                                                WHERE emprunts.fini = 0)');

        $this->execute($query, array($id));

        return $query->fetchAll();
    }


    /**
     * Getting the copy of a book
     */
    public function getCopy($copy)
    {
        $query = $this->pdo->prepare('SELECT exemplaires.id 
                                        FROM exemplaires 
                                        WHERE exemplaires.id = ?');

        $this->execute($query, array($copy));

        return $query->fetchAll();
    }

    /**
     * Return a book (Change field 'fini' 0 to 1 in the database)
     */
    public function returnBook($copy)
    {
        $query = $this->pdo->prepare('UPDATE emprunts SET fini=1 WHERE exemplaire = ? AND fini = 0');

        $this->execute($query, array($copy));
    }
}
