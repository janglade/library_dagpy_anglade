<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

$app->match('/books/{id}', function($id) use ($app) {
    
    if($id != -1) { // if there is a ID in the URL or not
        $request = $app['request'];
        $success = false;
        if ($request->getMethod() == 'POST') {
            $post = $request->request;
            if ($post->has('copyReturn')) {
                // Return the book
                $app['model']->returnBook($post->get('copyReturn'));  
                $success = true;
            }
        }

        return $app['twig']->render('books.html.twig', array(
            'book' => $app['model']->getBook($id),
            'copiesAvailable' => $app['model']->getAvailablesCopies($id),
            'copiesUnavailable' => $app['model']->getUnavailablesCopies($id),
            'success' => $success
        ));
    }
    else {
        return $app['twig']->render('books.html.twig', array(
            'books' => $app['model']->getBooks()
        ));
    }
})->value('id', -1)->bind('books');

$app->match('/books/{id}/borrow/{copy}', function($id, $copy) use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('name') && $post->has('end')) {
            // Saving the book borrowing in the database
            $app['model']->insertBorrow($post->get('name'), $copy, $post->get('end'));  
            $success = true;
        }
    }

    return $app['twig']->render('borrow.html.twig', array(
        'book' => $app['model']->getBook($id),
        'copy' => $app['model']->getCopy($copy),
        'success' => $success
    ));

})->bind('borrow');

$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password')) {
            $admins = $app['config']['admin'];
            foreach ($admins as $admin) {
                if ($post->get('login') == $admin[0] && $post->get('password') == $admin[1]) {
                    $app['session']->set('admin', true);
                    $success = true;
                }
            }  
        }
    }
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    $success = false;
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');

$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig');
})->bind('addBook');

